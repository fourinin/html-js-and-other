"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PHRASES = [
    { value: "Hello World" },
    { value: "Привет Мир" },
    { value: "Привіт Світ" },
    { value: "Hola Mundo" },
    { value: "Bonjour le monde" },
    { value: "Hallo Welt" },
    { value: "Ciao mondo" },
    { value: "Witaj świecie" },
    { value: "Hej världen" },
    { value: "Pozdravljen, svet" },
    { value: "Прывітанне Сусвет" }
];
var IntroductionComponent = (function () {
    function IntroductionComponent() {
        this.PhraseList = PHRASES;
    }
    return IntroductionComponent;
}());
IntroductionComponent = __decorate([
    core_1.Component({
        selector: "my-introduction",
        //template:"<h1>Introduction 1</h1>"
        templateUrl: "app/Home-Work/1_Introduction/1_Introduction.component.html",
        styleUrls: ["app/Home-Work/1_Introduction/1_Introduction.component.css"]
    })
], IntroductionComponent);
exports.IntroductionComponent = IntroductionComponent;
//# sourceMappingURL=1_Introduction.component.js.map