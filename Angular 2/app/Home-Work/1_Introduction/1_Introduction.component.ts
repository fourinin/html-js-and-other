import { Component } from "@angular/core";
import { Hello } from "./hello";

const PHRASES: Hello[] = [
    { value: "Hello World" },
    { value: "Привет Мир" },
    { value: "Привіт Світ" },
    { value: "Hola Mundo" },
    { value: "Bonjour le monde" },
    { value: "Hallo Welt" },
    { value: "Ciao mondo" },
    { value: "Witaj świecie" },
    { value: "Hej världen" },
    { value: "Pozdravljen, svet" },
    { value: "Прывітанне Сусвет" }
];

@Component({
    selector: "my-introduction", 
    //template:"<h1>Introduction 1</h1>"
    templateUrl: "app/Home-Work/1_Introduction/1_Introduction.component.html",
    styleUrls:["app/Home-Work/1_Introduction/1_Introduction.component.css"]
})
export class IntroductionComponent {
    PhraseList: Hello[] = PHRASES;
}

