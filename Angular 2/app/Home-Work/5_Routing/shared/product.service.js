"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_1 = require("../Products/product/product");
var ProductService = (function () {
    function ProductService() {
        this.newItem = null;
        this.CATEGORY = [
            "Category 1",
            "Category 2",
            "Category 3"
        ];
        this.PRODUCTS = [
            { id: 1, name: "product 1", price: 100, category: this.CATEGORY[0] },
            { id: 2, name: "product 2", price: 200, category: this.CATEGORY[1] },
            { id: 3, name: "product 3", price: 300, category: this.CATEGORY[2] },
            { id: 4, name: "product 4", price: 400, category: this.CATEGORY[0] },
            { id: 5, name: "product 5", price: 500, category: this.CATEGORY[1] },
            { id: 6, name: "product 6", price: 600, category: this.CATEGORY[2] },
            { id: 7, name: "product 7", price: 700, category: this.CATEGORY[0] },
            { id: 8, name: "product 8", price: 800, category: this.CATEGORY[1] },
            { id: 9, name: "product 9", price: 900, category: this.CATEGORY[2] },
            { id: 10, name: "product 10", price: 1000, category: this.CATEGORY[0] },
        ];
        // Promise, который сразу переходит в состояние resolved с данными из массива products
        this.productsPromise = Promise.resolve(this.PRODUCTS);
    }
    // Метод для получения фразы по id. Возвращает Promise c экземпляром Phrase
    ProductService.prototype.getProduct = function (id) {
        return this.productsPromise
            .then(function (items) { return items.find(function (x) { return x.id == id; }); });
    };
    ProductService.prototype.getData = function () {
        var items = [];
        for (var i = 0; i < this.PRODUCTS.length; i++) {
            items[i] = this.PRODUCTS[i];
        }
        return items;
    };
    ProductService.prototype.getCategories = function () {
        var items = [];
        for (var i = 0; i < this.CATEGORY.length; i++) {
            items[i] = this.CATEGORY[i];
        }
        return items;
    };
    ProductService.prototype.selectCategory = function (categorySelect) {
        console.log(categorySelect);
    };
    ProductService.prototype.addProduct = function (name, price, category) {
        console.log("click addProduct: name:" + name + " price:" + price + " category:" + category);
        this.newItem = new product_1.Product(this.PRODUCTS[this.PRODUCTS.length - 1].id + 1, name, price, category);
        this.PRODUCTS.push(this.newItem);
    };
    return ProductService;
}());
ProductService = __decorate([
    core_1.Injectable()
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map