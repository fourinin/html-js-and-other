"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router"); // модуль для маршрутизации
var _5_Routing_component_1 = require("./5_Routing.component");
var hw_5_component_1 = require("./HW_5/hw-5.component");
var home_component_1 = require("./Home/home.component");
var products_component_1 = require("./Products/products.component");
var admin_component_1 = require("./Admin/admin.component");
var Lesson5RoutingModule = (function () {
    function Lesson5RoutingModule() {
    }
    return Lesson5RoutingModule;
}());
Lesson5RoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forChild([
                {
                    // Для того, чтобы компонент отображался в router-outlet Lesson5Component, a не в AppComponent
                    // необходимо выполнить настройку дочерних маршрутов с помощью инициализации свойства children
                    path: "",
                    component: _5_Routing_component_1.Lesson5Component,
                    children: [
                        {
                            path: "", component: hw_5_component_1.HW5Component,
                            children: [
                                { path: "home", component: home_component_1.HomeComponent },
                                { path: "products", component: products_component_1.ProductsComponent },
                                { path: "admin", component: admin_component_1.AdminComponent }
                            ]
                        }
                    ]
                }
            ])
        ],
        exports: [
            router_1.RouterModule
        ]
    })
], Lesson5RoutingModule);
exports.Lesson5RoutingModule = Lesson5RoutingModule;
//# sourceMappingURL=5_Routing-routing.module.js.map