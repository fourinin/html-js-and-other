"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var _5_Routing_component_1 = require("./5_Routing.component");
var _5_Routing_routing_module_1 = require("./5_Routing-routing.module");
var hw_5_module_1 = require("./HW_5/hw-5.module");
var product_service_1 = require("./shared/product.service");
var Lesson5Module = (function () {
    function Lesson5Module() {
    }
    return Lesson5Module;
}());
Lesson5Module = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            _5_Routing_routing_module_1.Lesson5RoutingModule,
            hw_5_module_1.HW5Module
        ],
        declarations: [
            _5_Routing_component_1.Lesson5Component
        ],
        providers: [
            product_service_1.ProductService
        ]
    })
], Lesson5Module);
exports.Lesson5Module = Lesson5Module;
//# sourceMappingURL=5_Routing.module.js.map