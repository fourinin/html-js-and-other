import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router"; // модуль для маршрутизации

import { Lesson5Component } from "./5_Routing.component";
import { HW5Component } from "./HW_5/hw-5.component";
import { HomeComponent } from "./Home/home.component";
import { ProductsComponent } from "./Products/products.component";
import { AdminComponent } from "./Admin/admin.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                // Для того, чтобы компонент отображался в router-outlet Lesson5Component, a не в AppComponent
                // необходимо выполнить настройку дочерних маршрутов с помощью инициализации свойства children
                path: "",
                component: Lesson5Component, // содержит <router-outlet>
                children: [
                    {
                        path: "", component: HW5Component, // содержит <router-outlet>
                        children: [
                            { path: "home", component: HomeComponent },
                            { path: "products", component: ProductsComponent },
                            { path: "admin", component: AdminComponent }
                        ]
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class Lesson5RoutingModule { }