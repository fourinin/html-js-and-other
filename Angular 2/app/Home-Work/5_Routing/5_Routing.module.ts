import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Lesson5Component } from "./5_Routing.component";
import { HW5Component } from "./HW_5/hw-5.component";

import { Lesson5RoutingModule } from "./5_Routing-routing.module";
import { HW5Module } from "./HW_5/hw-5.module";

import { ProductService } from "./shared/product.service";

@NgModule({
  imports: [ /*modules*/
    CommonModule,
    Lesson5RoutingModule, // настройки маршрутизации для Lesson5Module
    HW5Module
  ],
  declarations: [ /*components*/
    Lesson5Component
  ],
  providers: [ /*services*/
    ProductService
  ]
})
export class Lesson5Module {

}