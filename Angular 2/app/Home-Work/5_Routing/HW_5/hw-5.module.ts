import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HW5Component } from "./hw-5.component";
import { HomeComponent } from "../Home/home.component";
import { ProductsComponent } from "../Products/products.component";
import { AdminComponent } from "../Admin/admin.component";
import { ProductComponent } from "../Products/product/product.component";
import { ProductDetailsComponent } from "../Products/product-details/product-details.component";

import { HW5RoutingModule } from "./hw-5-routing.module";

@NgModule({
  imports: [
    CommonModule,
    HW5RoutingModule, // настройки маршрутизации для модуля PhrasesModule
  ],
  declarations: [
    HW5Component,
    HomeComponent,
    ProductsComponent,
    AdminComponent,
    ProductComponent,
    ProductDetailsComponent
  ]
})
export class HW5Module { }