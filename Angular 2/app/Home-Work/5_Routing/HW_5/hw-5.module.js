"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var hw_5_component_1 = require("./hw-5.component");
var home_component_1 = require("../Home/home.component");
var products_component_1 = require("../Products/products.component");
var admin_component_1 = require("../Admin/admin.component");
var product_component_1 = require("../Products/product/product.component");
var product_details_component_1 = require("../Products/product-details/product-details.component");
var hw_5_routing_module_1 = require("./hw-5-routing.module");
var HW5Module = (function () {
    function HW5Module() {
    }
    return HW5Module;
}());
HW5Module = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            hw_5_routing_module_1.HW5RoutingModule,
        ],
        declarations: [
            hw_5_component_1.HW5Component,
            home_component_1.HomeComponent,
            products_component_1.ProductsComponent,
            admin_component_1.AdminComponent,
            product_component_1.ProductComponent,
            product_details_component_1.ProductDetailsComponent
        ]
    })
], HW5Module);
exports.HW5Module = HW5Module;
//# sourceMappingURL=hw-5.module.js.map