import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router"

import { Product } from "./product/product";
import { ProductService } from "../shared/product.service";

@Component({
    moduleId: module.id,
    selector: "products",
    templateUrl: "products.component.html",
    styleUrls: ["products.component.css"],
    inputs: ["rowCount", "id", "name", "price", "category"]
})
export class ProductsComponent {
    taskInfo: boolean = false;
    newProduct: boolean = false;
    newItem: Product = null;
    rowCount: Number;

    // ActivatedRoute - содержит информацию о маршруте связанную с компонентом, который загружен в outlet
    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private productService: ProductService) { }

    aboutProduct(item){
        this.router.navigate(["/products", item.id]);        
    }
}