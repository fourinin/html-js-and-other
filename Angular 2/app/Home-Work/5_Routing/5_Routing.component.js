"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
var Lesson5Component = (function () {
    function Lesson5Component() {
    }
    return Lesson5Component;
}());
Lesson5Component = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "less-5",
        templateUrl: "5_Routing.component.html",
        styleUrls: ["5_Routing.component.css"]
    })
], Lesson5Component);
exports.Lesson5Component = Lesson5Component;
//# sourceMappingURL=5_Routing.component.js.map