import { Component } from "@angular/core";

// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
@Component({
    moduleId: module.id,
    selector: "less-5",
    templateUrl: "5_Routing.component.html",
    styleUrls: ["5_Routing.component.css"]
})
export class Lesson5Component {
}

