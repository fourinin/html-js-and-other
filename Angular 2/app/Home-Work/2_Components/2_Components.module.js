"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router"); // модуль для маршрутизации
var product_component_1 = require("./product.component");
var task_1_component_1 = require("./task-1/task-1.component");
var task_2_component_1 = require("./task-2/task-2.component");
var task_3_component_1 = require("./task-3/task-3.component");
var Lesson2Module = (function () {
    function Lesson2Module() {
    }
    return Lesson2Module;
}());
Lesson2Module = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            router_1.RouterModule.forRoot([
                { path: "Leson2/Task1", component: task_1_component_1.Task1Component },
                { path: "Leson2/Task2", component: task_2_component_1.Task2Component },
                { path: "Leson2/Task3", component: task_3_component_1.Task3Component },
                { path: "", redirectTo: "Leson2/Task1", pathMatch: "full" }
            ])
        ],
        declarations: [
            product_component_1.ProductComponent,
            task_1_component_1.Task1Component,
            task_2_component_1.Task2Component,
            task_3_component_1.Task3Component
        ],
        exports: [
            task_1_component_1.Task1Component,
            task_2_component_1.Task2Component,
            task_3_component_1.Task3Component
        ],
        providers: []
    })
], Lesson2Module);
exports.Lesson2Module = Lesson2Module;
//# sourceMappingURL=2_Components.module.js.map