import { Component } from "@angular/core";

// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
@Component({
    moduleId: module.id,
    selector: "less-2",
    templateUrl: "2_Components.component.html",
    styleUrls: ["2_Components.component.css"]
})
export class Lesson2Component {
    
}

