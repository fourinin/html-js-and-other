import { Component, ViewChild  } from "@angular/core";

import { ProductComponent3 } from "./product.component";
import { Products } from "../products";

const PRODUCTS: Products[] = [
    { id: 1, name:"product 1", price: 100},
    { id: 2, name:"product 2", price: 200},
    { id: 3, name:"product 3", price: 300},
    { id: 4, name:"product 4", price: 400},
    { id: 5, name:"product 5", price: 500},
    { id: 6, name:"product 6", price: 600},
    { id: 7, name:"product 7", price: 700},
    { id: 8, name:"product 8", price: 800},
    { id: 9, name:"product 9", price: 900},
    { id: 10, name:"product 10", price: 1000},
];

@Component({
    moduleId: module.id,
    selector: "task-3",
    templateUrl: "task-3.component.html",
    styleUrls: ["task-3.component.css"]
})
export class Task3Component {
    //@ViewChild(ProductComponent) // Для получения доступа к дочернему компоненту
    product: ProductComponent3;

    ProductList: Products[] = PRODUCTS;

    deleteProduct()
    {
        console.log("product with id:"+this.product.id+" name:"+this.product.name+" price:"+this.product.price+" was DELETED!");
        this.product.hide();
    }
}