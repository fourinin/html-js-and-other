"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProductComponent3 = (function () {
    function ProductComponent3() {
        this.visible = true;
    }
    ProductComponent3.prototype.show = function () {
        this.visible = true;
    };
    ProductComponent3.prototype.hide = function () {
        this.visible = false;
    };
    return ProductComponent3;
}());
ProductComponent3 = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "product",
        templateUrl: "product.component.html",
        inputs: ["id", "name", "price"]
    })
], ProductComponent3);
exports.ProductComponent3 = ProductComponent3;
//# sourceMappingURL=product.component.js.map