import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "product",
    templateUrl: "product.component.html",
    inputs: ["id","name", "price"]
})
export class ProductComponent3 {
    private visible: boolean = true;
    id: number;
    name:string;
    price: number;

    show() {
        this.visible = true;
    } 

    hide() {
        this.visible = false;
    } 
}