import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router"; // модуль для маршрутизации

import { Lesson2Component } from "./2_Components.component";
import { ProductComponent } from "./product.component";

import { Task1Component } from "./task-1/task-1.component";
import { Task2Component } from "./task-2/task-2.component";
import { Task3Component } from "./task-3/task-3.component";
import { ProductComponent3 } from "./task-3/product.component";

@NgModule({
  imports:      [ /*modules*/ 
    BrowserModule,
    RouterModule.forRoot([ // настройка маршрутов
            { path: "Leson2/Task1", component: Task1Component }, 
            { path: "Leson2/Task2", component: Task2Component }, 
            { path: "Leson2/Task3", component: Task3Component },
            { path: "", redirectTo: "Leson2/Task1", pathMatch: "full" }
        ])
    ],
  declarations: [ /*components*/ 
    ProductComponent,
    Task1Component,
    Task2Component,
    Task3Component
  ],
  exports: [
    Task1Component,
    Task2Component,
    Task3Component
  ],
  providers:    [ /*services*/ ]
})
export class Lesson2Module { 

}