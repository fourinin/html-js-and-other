"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router"); // модуль для маршрутизации
var product_component_1 = require("./product.component");
var task_1_component_1 = require("./task-1/task-1.component");
//import { Task2Component } from "./task-2/task-2.component";
var product_service_1 = require("./product.service");
var Lesson4Module = (function () {
    function Lesson4Module() {
    }
    return Lesson4Module;
}());
Lesson4Module = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            router_1.RouterModule.forRoot([
                { path: "Leson4/Task1+Task2", component: task_1_component_1.Task1Component },
                { path: "", redirectTo: "Leson4/Task1", pathMatch: "full" }
            ])
        ],
        declarations: [
            product_component_1.ProductComponent,
            task_1_component_1.Task1Component
        ],
        exports: [
            task_1_component_1.Task1Component
        ],
        providers: [
            product_service_1.ProductService
        ]
    })
], Lesson4Module);
exports.Lesson4Module = Lesson4Module;
//# sourceMappingURL=4_Services.module.js.map