import { Component } from "@angular/core";

// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
@Component({
    moduleId: module.id,
    selector: "less-4",
    templateUrl: "4_Services.component.html",
    styleUrls: ["4_Services.component.css"]
})
export class Lesson4Component {
    
}

