import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "product",
    templateUrl: "product.component.html",
    inputs: ["id","name", "price", "category"]
})
export class ProductComponent {
    private visible: boolean = true;
    id: number;
    name:string;
    price: number;
    category: string;

    show() {
        this.visible = true;
    } 

    hide() {
        this.visible = false;
    } 
}