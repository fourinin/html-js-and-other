import { Injectable } from "@angular/core";
import { Product } from "./product";

@Injectable()
export class ProductService {
    newItem : Product = null;
    private CATEGORY: string[] = [
        "Category 1",
        "Category 2",
        "Category 3"
    ];
    private PRODUCTS: Product[] = [
        { id: 1, name: "product 1", price: 100, category: this.CATEGORY[0] },
        { id: 2, name: "product 2", price: 200, category: this.CATEGORY[1] },
        { id: 3, name: "product 3", price: 300, category: this.CATEGORY[2] },
        { id: 4, name: "product 4", price: 400, category: this.CATEGORY[0] },
        { id: 5, name: "product 5", price: 500, category: this.CATEGORY[1] },
        { id: 6, name: "product 6", price: 600, category: this.CATEGORY[2] },
        { id: 7, name: "product 7", price: 700, category: this.CATEGORY[0] },
        { id: 8, name: "product 8", price: 800, category: this.CATEGORY[1] },
        { id: 9, name: "product 9", price: 900, category: this.CATEGORY[2] },
        { id: 10, name: "product 10", price: 1000, category: this.CATEGORY[0] },
    ];

    getData() {
        let items: Product[] = [];

        for (var i = 0; i < this.PRODUCTS.length; i++) {
            items[i] = this.PRODUCTS[i];
        }
        return items;
    }

    getCategories() {
        let items: string[] = [];

        for (var i = 0; i < this.CATEGORY.length; i++) {
            items[i] = this.CATEGORY[i];
        }
        return items;
    }

    selectCategory(categorySelect){
        console.log(categorySelect);
    }

    addProduct(name, price, category){
        console.log("click addProduct: name:" + name + " price:"+price+" category:"+category);
            this.newItem = new Product(this.PRODUCTS[this.PRODUCTS.length-1].id+1, name, price, category);
            this.PRODUCTS.push(this.newItem);
    }

}