import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router"; // модуль для маршрутизации

import { Lesson4Component } from "./4_Services.component";
import { ProductComponent } from "./product.component";

import { Task1Component } from "./task-1/task-1.component";
//import { Task2Component } from "./task-2/task-2.component";

import { ProductService } from "./product.service";

@NgModule({
  imports: [ /*modules*/
    BrowserModule,
    RouterModule.forRoot([ // настройка маршрутов
      { path: "Leson4/Task1+Task2", component: Task1Component },
      { path: "", redirectTo: "Leson4/Task1", pathMatch: "full" }
    ])
  ],
  declarations: [ /*components*/
    ProductComponent,
    Task1Component
  ],
  exports: [
    Task1Component
  ],
  providers: [ /*services*/
    ProductService
  ]
})
export class Lesson4Module {

}