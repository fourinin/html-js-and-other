"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_1 = require("../product");
var product_service_1 = require("../product.service");
var CATEGORY = [
    "Category 1"
];
var PRODUCTS = [
    { id: 1, name: "product 1", price: 100, category: CATEGORY[0] }
];
var Task1Component = (function () {
    function Task1Component(productService) {
        this.productService = productService;
        this.taskInfo = false;
        this.newProduct = false;
        this.newItem = null;
        this.ProductList = PRODUCTS;
        this.CategoryList = CATEGORY;
    }
    Task1Component.prototype.addProduct = function (name, price, category) {
        console.log("click addProduct: name:" + name + " price:" + price + " category:" + category);
        //this.ProductList.push(new Product(10, name, price, category ));
        this.newItem = new product_1.Product(this.ProductList[this.ProductList.length - 1].id + 1, name, price, category);
        this.ProductList.push(this.newItem);
    };
    return Task1Component;
}());
Task1Component = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "task-1",
        templateUrl: "task-1.component.html",
        styleUrls: ["task-1.component.css"],
        inputs: ["rowCount"]
    }),
    __metadata("design:paramtypes", [product_service_1.ProductService])
], Task1Component);
exports.Task1Component = Task1Component;
//# sourceMappingURL=task-1.component.js.map