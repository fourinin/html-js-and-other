import { Component } from "@angular/core";

import { Product } from "../product";

import { ProductService } from "../product.service";

const CATEGORY: string[] = [
    "Category 1"
];
const PRODUCTS: Product[] = [
    { id: 1, name:"product 1", price: 100, category: CATEGORY[0]}
];

@Component({
    moduleId: module.id,
    selector: "task-1",
    templateUrl: "task-1.component.html",
    styleUrls: ["task-1.component.css"],
    inputs:["rowCount"]
})
export class Task1Component {
    taskInfo : boolean = false;
    newProduct : boolean = false;
    newItem : Product = null;
    rowCount : Number;
    ProductList: Product[] = PRODUCTS;
    CategoryList: string[] = CATEGORY;

    constructor(private productService: ProductService){}

    addProduct(name, price, category){
        console.log("click addProduct: name:" + name + " price:"+price+" category:"+category);

        //this.ProductList.push(new Product(10, name, price, category ));

            this.newItem = new Product(this.ProductList[this.ProductList.length-1].id+1, name, price, category);
            this.ProductList.push(this.newItem);
    }
}