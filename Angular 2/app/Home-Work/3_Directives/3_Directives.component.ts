import { Component } from "@angular/core";

// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
@Component({
    moduleId: module.id,
    selector: "less-3",
    templateUrl: "3_Directives.component.html",
    styleUrls: ["3_Directives.component.css"]
})
export class Lesson3Component {
    
}

