import { Component } from "@angular/core";

import { Product } from "../product";

const CATEGORY: string[] = [
    "Category 1",
    "Category 2",
    "Category 3"
];

const PRODUCTS: Product[] = [
    { id: 1, name:"product 1", price: 100, category: CATEGORY[0]},
    { id: 2, name:"product 2", price: 200, category: CATEGORY[1]},
    { id: 3, name:"product 3", price: 300, category: CATEGORY[2]},
    { id: 4, name:"product 4", price: 400, category: CATEGORY[0]},
    { id: 5, name:"product 5", price: 500, category: CATEGORY[1]},
    { id: 6, name:"product 6", price: 600, category: CATEGORY[2]},
    { id: 7, name:"product 7", price: 700, category: CATEGORY[0]},
    { id: 8, name:"product 8", price: 800, category: CATEGORY[1]},
    { id: 9, name:"product 9", price: 900, category: CATEGORY[2]},
    { id: 10, name:"product 10", price: 1000, category: CATEGORY[0]},
];

@Component({
    moduleId: module.id,
    selector: "task-2",
    templateUrl: "task-2.component.html",
    styleUrls: ["task-2.component.css"],
    inputs:["rowCount"]
})
export class Task2Component {
    rowCount : Number;
    ProductList: Product[] = PRODUCTS;
    CategoryList: string[] = CATEGORY;

    selectCategory(categorySelect){
        console.log(categorySelect);
    }
}