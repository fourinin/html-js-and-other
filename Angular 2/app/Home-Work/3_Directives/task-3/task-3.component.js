"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var product_1 = require("../product");
var CATEGORY = [
    "Category 1",
    "Category 2",
    "Category 3"
];
var PRODUCTS = [
    { id: 1, name: "product 1", price: 100, category: CATEGORY[0] },
    { id: 2, name: "product 2", price: 200, category: CATEGORY[1] },
    { id: 3, name: "product 3", price: 300, category: CATEGORY[2] },
    { id: 4, name: "product 4", price: 400, category: CATEGORY[0] },
    { id: 5, name: "product 5", price: 500, category: CATEGORY[1] },
    { id: 6, name: "product 6", price: 600, category: CATEGORY[2] },
    { id: 7, name: "product 7", price: 700, category: CATEGORY[0] },
    { id: 8, name: "product 8", price: 800, category: CATEGORY[1] },
    { id: 9, name: "product 9", price: 900, category: CATEGORY[2] },
    { id: 10, name: "product 10", price: 1000, category: CATEGORY[0] },
];
var Task3Component = (function () {
    function Task3Component() {
        this.taskInfo = false;
        this.newProduct = false;
        this.newItem = null;
        this.ProductList = PRODUCTS;
        this.CategoryList = CATEGORY;
    }
    Task3Component.prototype.selectCategory = function (categorySelect) {
        console.log(categorySelect);
    };
    Task3Component.prototype.addProduct = function (name, price, category) {
        console.log("click addProduct: name:" + name + " price:" + price + " category:" + category);
        //this.ProductList.push(new Product(10, name, price, category ));
        this.newItem = new product_1.Product(this.ProductList[this.ProductList.length - 1].id + 1, name, price, category);
        this.ProductList.push(this.newItem);
    };
    return Task3Component;
}());
Task3Component = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "task-3",
        templateUrl: "task-3.component.html",
        styleUrls: ["task-3.component.css"],
        inputs: ["rowCount"]
    })
], Task3Component);
exports.Task3Component = Task3Component;
//# sourceMappingURL=task-3.component.js.map