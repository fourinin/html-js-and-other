import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router"; // модуль для маршрутизации

import { Lesson6Component } from "./6_Forms.component";
import { HomeComponent } from "./Home/home.component";
import { ProductsComponent } from "./Products/products.component";
import { Product } from "./Products/product/product";
import { AdminComponent } from "./Admin/admin.component";
import { ProductDetailsComponent } from "./Products/product-details/product-details.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                // Для того, чтобы компонент отображался в router-outlet Lesson5Component, a не в AppComponent
                // необходимо выполнить настройку дочерних маршрутов с помощью инициализации свойства children
                path: "lesson_6",
                component: Lesson6Component, // содержит <router-outlet>
                children: [
                    { path: "home", component: HomeComponent },
                    { path: "products", component: ProductsComponent },
                    {
                        path: "products", component: ProductsComponent,
                        children: [
                            { path: ":productID", component: ProductDetailsComponent }
                        ]
                    },
                    { path: "admin", component: AdminComponent }
                ]

            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class Lesson6RoutingModule { }