import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";

import { Product } from "./Products/product/product";
import { ProductService } from "./shared/product.service";

// Для использования относительных путей, необходимо добавить свойство moduleId и установить значение для свойства module.id
// Данное свойство необходимо устанавливать в случае если в проекте используется загрузчик systemJS
@Component({
    moduleId: module.id,
    selector: "less-6",
    templateUrl: "6_Forms.component.html",
    styleUrls: ["6_Forms.component.css"]
})
export class Lesson6Component {

    // ActivatedRoute - содержит информацию о маршруте связанную с компонентом, который загружен в outlet
    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private productService: ProductService) { }
}

