"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router"); // модуль для маршрутизации
var _6_Forms_component_1 = require("./6_Forms.component");
var home_component_1 = require("./Home/home.component");
var products_component_1 = require("./Products/products.component");
var admin_component_1 = require("./Admin/admin.component");
var product_details_component_1 = require("./Products/product-details/product-details.component");
var Lesson6RoutingModule = (function () {
    function Lesson6RoutingModule() {
    }
    return Lesson6RoutingModule;
}());
Lesson6RoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forChild([
                {
                    // Для того, чтобы компонент отображался в router-outlet Lesson5Component, a не в AppComponent
                    // необходимо выполнить настройку дочерних маршрутов с помощью инициализации свойства children
                    path: "lesson_6",
                    component: _6_Forms_component_1.Lesson6Component,
                    children: [
                        { path: "home", component: home_component_1.HomeComponent },
                        { path: "products", component: products_component_1.ProductsComponent },
                        {
                            path: "products", component: products_component_1.ProductsComponent,
                            children: [
                                { path: ":productID", component: product_details_component_1.ProductDetailsComponent }
                            ]
                        },
                        { path: "admin", component: admin_component_1.AdminComponent }
                    ]
                }
            ])
        ],
        exports: [
            router_1.RouterModule
        ]
    })
], Lesson6RoutingModule);
exports.Lesson6RoutingModule = Lesson6RoutingModule;
//# sourceMappingURL=6_Forms-routing.module.js.map