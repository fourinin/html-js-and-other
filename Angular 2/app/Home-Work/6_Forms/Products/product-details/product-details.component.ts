import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Product } from "../product/product";
import { ProductService } from "../../shared/product.service";

@Component({
    moduleId: module.id,
    selector: "product-details",
    templateUrl: "product-details.component.html"
})
export class ProductDetailsComponent implements OnInit {

    product: Product;

    // ActivatedRoute - содержит информацию о маршруте связанную с компонентом, который загружен в outlet
    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private service: ProductService) { }

    ngOnInit() {
        // params - параметры текущего маршрута. Данное свойство является Observable объектом
        // Если параметры будут изменены - произойдет событие и компонент узнает о изменениях.

        // forEach - устанавливаем обработчик на каждое изменение params
        this.activatedRoute.params.forEach((params: Params) => {
            let id = +params["productID"]; // конвертируем значение параметра id в тип number
            this.service
                .getProduct(id)  // обращаемся к сервису и запрашиваем фразу по id. Получаем Promise
                .then(result => this.product = result);  // как только Promise перейдет в состояние resolved присваиваем его значение свойству phrase
        });
    }
}