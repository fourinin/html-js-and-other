import { Component, OnInit } from "@angular/core";
// ActivatedRoute - содержит информацию о маршруте связанную с компонентом, который загружен в outlet
import { Router, ActivatedRoute, Params } from "@angular/router"
// FormGroup - группа отдельных элементов управления (FormControl)
// FormControl - класс, который представляет элемент управления 
// Validators - класс, со статическими методами для валидции
// FormBuilder - класс, предоставляющий удобный интерфейс для создания экземпляров FormControl и FormGroup 
import { FormGroup, FormBuilder, Validators } from '@angular/forms'; //Для поддержки валидации

import { Product } from "./product/product";
import { ProductComponent } from "./product/product.component";
import { ProductService } from "../shared/product.service";
// Для работы с реактивными формами необходимо использовать модуль ReactiveFormsModule
// вместо ReactiveForms
import { ReactiveFormsModule } from "@angular/forms";

@Component({
    moduleId: module.id,
    selector: "products",
    templateUrl: "products.component.html",
    styleUrls: ["../../../../node_modules/bootstrap/dist/css/bootstrap.css",
        "products.component.css"],
    inputs: ["rowCount", "id", "name", "price", "category"]
})
export class ProductsComponent implements OnInit {
    taskInfo: boolean = false;
    newProduct: boolean = false;
    newProduct2: boolean = false;
    newItem: Product = new Product();
    rowCount: Number;

    // Урок 6 - Формы, валидация
    newProductForm: FormGroup;

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors = {
        "name": "",
        "price": ""
    };
    // Объект с сообщениями ошибок
    validationMessages = {
        "name": {
            "required": "Обязательное поле.",
            "minlength": "Значение должно быть не менее 4х символов.",
            "maxlength": "Значение не должно быть больше 15 символов."
        },
        "price": {
            "required": "Обязательное поле.",
            "maxlength": "Значение не должно быть больше 7 символов.",
            "pattern": "Значение должно быть целым числом."
        }
    };
    
    constructor(private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private productService: ProductService) { }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.newProductForm = this.fb.group({
            "name": [this.newItem.name, [
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(15)
            ]],
            "price": [this.newItem.price, [
                Validators.required,
                Validators.maxLength(7),
                Validators.pattern("\\d+")
            ]],
        });

        this.newProductForm.valueChanges
            .subscribe(data => this.onValueChange(data));

        this.onValueChange();
    }

    onValueChange(data?: any) {
        if (!this.newProductForm) return;
        let form = this.newProductForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

    onSubmit(form) {
        console.log("submitted!");
        console.log(this.newProductForm.value);
    }

    aboutProduct(item) {
        this.router.navigate(["/lesson_6/products", item.id]);
    }
}