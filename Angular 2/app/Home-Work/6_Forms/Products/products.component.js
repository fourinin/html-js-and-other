"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// ActivatedRoute - содержит информацию о маршруте связанную с компонентом, который загружен в outlet
var router_1 = require("@angular/router");
// FormGroup - группа отдельных элементов управления (FormControl)
// FormControl - класс, который представляет элемент управления 
// Validators - класс, со статическими методами для валидции
// FormBuilder - класс, предоставляющий удобный интерфейс для создания экземпляров FormControl и FormGroup 
var forms_1 = require("@angular/forms"); //Для поддержки валидации
var product_1 = require("./product/product");
var product_service_1 = require("../shared/product.service");
var ProductsComponent = (function () {
    function ProductsComponent(fb, router, activatedRoute, productService) {
        this.fb = fb;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.productService = productService;
        this.taskInfo = false;
        this.newProduct = false;
        this.newProduct2 = false;
        this.newItem = new product_1.Product();
        // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
        this.formErrors = {
            "name": "",
            "price": ""
        };
        // Объект с сообщениями ошибок
        this.validationMessages = {
            "name": {
                "required": "Обязательное поле.",
                "minlength": "Значение должно быть не менее 4х символов.",
                "maxlength": "Значение не должно быть больше 15 символов."
            },
            "price": {
                "required": "Обязательное поле.",
                "maxlength": "Значение не должно быть больше 7 символов.",
                "pattern": "Значение должно быть целым числом."
            }
        };
    }
    ProductsComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    ProductsComponent.prototype.buildForm = function () {
        var _this = this;
        this.newProductForm = this.fb.group({
            "name": [this.newItem.name, [
                    forms_1.Validators.required,
                    forms_1.Validators.minLength(4),
                    forms_1.Validators.maxLength(15)
                ]],
            "price": [this.newItem.price, [
                    forms_1.Validators.required,
                    forms_1.Validators.maxLength(7),
                    forms_1.Validators.pattern("\\d+")
                ]],
        });
        this.newProductForm.valueChanges
            .subscribe(function (data) { return _this.onValueChange(data); });
        this.onValueChange();
    };
    ProductsComponent.prototype.onValueChange = function (data) {
        if (!this.newProductForm)
            return;
        var form = this.newProductForm;
        for (var field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            var control = form.get(field);
            if (control && control.dirty && !control.valid) {
                var message = this.validationMessages[field];
                for (var key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    };
    ProductsComponent.prototype.onSubmit = function (form) {
        console.log("submitted!");
        console.log(this.newProductForm.value);
    };
    ProductsComponent.prototype.aboutProduct = function (item) {
        this.router.navigate(["/lesson_6/products", item.id]);
    };
    return ProductsComponent;
}());
ProductsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "products",
        templateUrl: "products.component.html",
        styleUrls: ["../../../../node_modules/bootstrap/dist/css/bootstrap.css",
            "products.component.css"],
        inputs: ["rowCount", "id", "name", "price", "category"]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        router_1.Router,
        router_1.ActivatedRoute,
        product_service_1.ProductService])
], ProductsComponent);
exports.ProductsComponent = ProductsComponent;
//# sourceMappingURL=products.component.js.map