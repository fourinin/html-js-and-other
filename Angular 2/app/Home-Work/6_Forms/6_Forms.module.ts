import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Lesson6Component } from "./6_Forms.component";
import { HomeComponent } from "./Home/home.component";
import { ProductsComponent } from "./Products/products.component";
import { ProductComponent } from "./Products/product/product.component";
import { AdminComponent } from "./Admin/admin.component";
import { ProductDetailsComponent } from "./Products/product-details/product-details.component";

// Для работы с реактивными формами необходимо использовать модуль ReactiveFormsModule
// вместо ReactiveForms
import { ReactiveFormsModule } from "@angular/forms";
import { Lesson6RoutingModule } from "./6_Forms-routing.module";

import { ProductService } from "./shared/product.service";

@NgModule({
  imports: [ /*modules*/
    CommonModule,
    Lesson6RoutingModule, // настройки маршрутизации для Lesson5Module
    ReactiveFormsModule
  ],
  declarations: [ /*components*/
    Lesson6Component,
    HomeComponent,
    ProductsComponent,
    ProductComponent,
    AdminComponent,
    ProductDetailsComponent
  ],
  providers: [ /*services*/
    ProductService
  ]
})
export class Lesson6Module {

}