"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var _6_Forms_component_1 = require("./6_Forms.component");
var home_component_1 = require("./Home/home.component");
var products_component_1 = require("./Products/products.component");
var product_component_1 = require("./Products/product/product.component");
var admin_component_1 = require("./Admin/admin.component");
var product_details_component_1 = require("./Products/product-details/product-details.component");
// Для работы с реактивными формами необходимо использовать модуль ReactiveFormsModule
// вместо ReactiveForms
var forms_1 = require("@angular/forms");
var _6_Forms_routing_module_1 = require("./6_Forms-routing.module");
var product_service_1 = require("./shared/product.service");
var Lesson6Module = (function () {
    function Lesson6Module() {
    }
    return Lesson6Module;
}());
Lesson6Module = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            _6_Forms_routing_module_1.Lesson6RoutingModule,
            forms_1.ReactiveFormsModule
        ],
        declarations: [
            _6_Forms_component_1.Lesson6Component,
            home_component_1.HomeComponent,
            products_component_1.ProductsComponent,
            product_component_1.ProductComponent,
            admin_component_1.AdminComponent,
            product_details_component_1.ProductDetailsComponent
        ],
        providers: [
            product_service_1.ProductService
        ]
    })
], Lesson6Module);
exports.Lesson6Module = Lesson6Module;
//# sourceMappingURL=6_Forms.module.js.map