import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router"; // модуль для маршрутизации

import { AppComponent } from "./app.component";

//homework modules
import { AppRoutingModule } from "./app-routing.module";
import { Lesson2Module } from "./Home-Work/2_Components/2_Components.module";
import { Lesson3Module } from "./Home-Work/3_Directives/3_Directives.module";
import { Lesson4Module } from "./Home-Work/4_Services/4_Services.module";
import { Lesson5Module } from "./Home-Work/5_Routing/5_Routing.module";
import { Lesson6Module } from "./Home-Work/6_Forms/6_Forms.module";

//homework components
import { IntroductionComponent } from "./Home-Work/1_Introduction/1_Introduction.component";
import { Lesson2Component } from "./Home-Work/2_Components/2_Components.component";
import { Lesson3Component } from "./Home-Work/3_Directives/3_Directives.component";
import { Lesson4Component } from "./Home-Work/4_Services/4_Services.component";

//additional
import { AdditionalComponent } from "./Additional/Additional.component";

@NgModule({
    imports: [ /*modules*/
        BrowserModule,
        AppRoutingModule, 
        Lesson2Module,
        Lesson3Module,
        Lesson4Module,
        Lesson5Module,
        Lesson6Module  
    ],
    declarations: [ /*components*/
        AppComponent, //main component
        IntroductionComponent, //Lesson 1 component
        Lesson2Component,
        Lesson3Component,
        Lesson4Component,
        AdditionalComponent
        ],  
    //exports: [IntroductionComponent],
    bootstrap: [AppComponent]
})
export class AppModule {

}