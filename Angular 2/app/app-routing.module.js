"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//homework components
var _1_Introduction_component_1 = require("./Home-Work/1_Introduction/1_Introduction.component");
var _2_Components_component_1 = require("./Home-Work/2_Components/2_Components.component");
var _3_Directives_component_1 = require("./Home-Work/3_Directives/3_Directives.component");
var _4_Services_component_1 = require("./Home-Work/4_Services/4_Services.component");
var _5_Routing_component_1 = require("./Home-Work/5_Routing/5_Routing.component");
var _6_Forms_component_1 = require("./Home-Work/6_Forms/6_Forms.component");
//additional
var Additional_component_1 = require("./Additional/Additional.component");
// В данном примере настройки маршрутизации выделены в отдельный модуль.
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot([
                // при переходе по адресу localhost:3000/les1 должен активироваться компонент IntroductionComponent
                { path: "les1", component: _1_Introduction_component_1.IntroductionComponent },
                { path: "les2", component: _2_Components_component_1.Lesson2Component },
                { path: "les3", component: _3_Directives_component_1.Lesson3Component },
                { path: "les4", component: _4_Services_component_1.Lesson4Component },
                { path: "les5", component: _5_Routing_component_1.Lesson5Component },
                { path: "les6", component: _6_Forms_component_1.Lesson6Component },
                { path: "additional", component: Additional_component_1.AdditionalComponent },
                { path: "", redirectTo: "les1", pathMatch: "full" }
            ])],
        exports: [router_1.RouterModule] // делаем re-export модуля для использования директив при маршрутизации
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map