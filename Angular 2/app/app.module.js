"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
//homework modules
var app_routing_module_1 = require("./app-routing.module");
var _2_Components_module_1 = require("./Home-Work/2_Components/2_Components.module");
var _3_Directives_module_1 = require("./Home-Work/3_Directives/3_Directives.module");
var _4_Services_module_1 = require("./Home-Work/4_Services/4_Services.module");
var _5_Routing_module_1 = require("./Home-Work/5_Routing/5_Routing.module");
var _6_Forms_module_1 = require("./Home-Work/6_Forms/6_Forms.module");
//homework components
var _1_Introduction_component_1 = require("./Home-Work/1_Introduction/1_Introduction.component");
var _2_Components_component_1 = require("./Home-Work/2_Components/2_Components.component");
var _3_Directives_component_1 = require("./Home-Work/3_Directives/3_Directives.component");
var _4_Services_component_1 = require("./Home-Work/4_Services/4_Services.component");
//additional
var Additional_component_1 = require("./Additional/Additional.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            app_routing_module_1.AppRoutingModule,
            _2_Components_module_1.Lesson2Module,
            _3_Directives_module_1.Lesson3Module,
            _4_Services_module_1.Lesson4Module,
            _5_Routing_module_1.Lesson5Module,
            _6_Forms_module_1.Lesson6Module
        ],
        declarations: [
            app_component_1.AppComponent,
            _1_Introduction_component_1.IntroductionComponent,
            _2_Components_component_1.Lesson2Component,
            _3_Directives_component_1.Lesson3Component,
            _4_Services_component_1.Lesson4Component,
            Additional_component_1.AdditionalComponent
        ],
        //exports: [IntroductionComponent],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map