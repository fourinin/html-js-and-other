import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "my-app",
    //template: "<h1>Hello world !</h1>"
    //templateUrl:"app/app.component.html"
    templateUrl: "app.component.html",
    styleUrls: ["app.component.css"]
})
export class AppComponent{

}