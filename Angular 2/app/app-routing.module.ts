import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

//homework components
import { IntroductionComponent } from "./Home-Work/1_Introduction/1_Introduction.component";
import { Lesson2Component } from "./Home-Work/2_Components/2_Components.component";
import { Lesson3Component } from "./Home-Work/3_Directives/3_Directives.component";
import { Lesson4Component } from "./Home-Work/4_Services/4_Services.component";
import { Lesson5Component } from "./Home-Work/5_Routing/5_Routing.component";
import { Lesson6Component } from "./Home-Work/6_Forms/6_Forms.component";

//additional
import { AdditionalComponent } from "./Additional/Additional.component";

// В данном примере настройки маршрутизации выделены в отдельный модуль.

@NgModule({
    imports: [RouterModule.forRoot([ // настройка маршрутов
            // при переходе по адресу localhost:3000/les1 должен активироваться компонент IntroductionComponent
            { path: "les1", component: IntroductionComponent }, 
            { path: "les2", component: Lesson2Component },
            { path: "les3", component: Lesson3Component },
            { path: "les4", component: Lesson4Component },
            { path: "les5", component: Lesson5Component },
            { path: "les6", component: Lesson6Component },
            { path: "additional", component: AdditionalComponent },
            { path: "", redirectTo: "les1", pathMatch: "full" }
        ])],
    exports: [RouterModule] // делаем re-export модуля для использования директив при маршрутизации
})
export class AppRoutingModule { }